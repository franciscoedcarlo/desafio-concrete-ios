//
//  DesafioConcreteTests.swift
//  DesafioConcreteTests
//
//  Created by Francisco on 05/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import XCTest
@testable import DesafioConcrete


class DesafioConcreteTests: XCTestCase {
    let master = MasterViewController()
    let listRepository = [RepositoryModel]()
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSetListRepository() {
        
        master.setListRepository(list: listRepository)
        
        XCTAssert(master.objects.count == listRepository.count)
    }
}
