//
//  PullRequestViewCell.swift
//  DesafioConcrete
//
//  Created by Francisco on 05/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import UIKit
import SDWebImage

class PullRequestViewCell: UITableViewCell {
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPullRequest(pull: PullModel){
        self.lblTitulo.text = pull.Title
        self.lblDescription.text = pull.Description
        self.lblUser.text = pull.User?.UserLogin
        self.lblDate.text = formatDate(date: pull.Created!)
        self.imgAvatar.sd_setImage(with: URL(string: (pull.User?.AvatarURL)!))
    }
    
    func formatDate(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let d = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy, H:mm"
        return dateFormatter.string(from: d!)
    }

}
