//
//  TableViewCell.swift
//  DesafioConcrete
//
//  Created by Francisco on 04/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryViewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblForks: UILabel!
    @IBOutlet weak var lblStar: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!

    @IBOutlet weak var lblUser: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setRepository(rep : RepositoryModel){
        self.lblTitle.text = rep.RepositoryName
        self.lblDescription.text = rep.Description
        self.lblForks.text = rep.Forks?.description
        self.lblStar.text = rep.Stars?.description
        self.imgAvatar.sd_setImage(with: URL(string: (rep.Owner?.AvatarURL)!))
        self.lblName.text = rep.Repository
        self.lblUser.text = rep.Owner?.UserLogin
    }

}
