//
//  RepositoryPresenter.swift
//  DesafioConcrete
//
//  Created by Francisco on 04/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class RepositoryPresenter : RepositoryPresenterDelegate{
    var viewDelegate : ListMainViewDelegate?
    var viewDetailDelegate : DetailViewDelegate?
    let endPointRepositories = "https://api.github.com/search/repositories"
    var listRepository = [RepositoryModel]()
    
    func getRepository(page: Int){
        let parameters = ["q": "language:Java",
                          "sort": "stars",
                          "page": page] as [String : Any]
        
        Alamofire.request( endPointRepositories, method: .get, parameters: parameters )
            .responseObject { (response : DataResponse<RepositoryListModel> ) in
                let list = response.result.value
                
                if let items = list?.items {
                    self.viewDelegate?.setListRepository(list: items)
                } else {
                    self.viewDelegate?.showMessage(msg: "Não foi possível recuperar os repositórios. Verifique sua conexão com a internet e tente novamente.")
                }
        }
    }
    
    func getPullRequest(rep: RepositoryModel) {
        let endPointPullRequest = "https://api.github.com/repos/\((rep.Owner?.UserLogin)!)/\(rep.Repository!)/pulls"
        Alamofire.request( endPointPullRequest, method: .get, parameters: nil )
            .responseArray { (response : DataResponse<[PullModel]> ) in
                if let list = response.result.value{
                    self.viewDetailDelegate?.setDetailObject(pull: list)
                } else {
                    self.viewDetailDelegate?.showMessage(msg: "Não foi possível recuperar o pull request. Verifique sua conexão com a internet e tente novamente.")
                }
            }
    }
}
