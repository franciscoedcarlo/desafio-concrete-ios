//
//  MasterViewController.swift
//  DesafioConcrete
//
//  Created by Francisco on 04/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, ListMainViewDelegate {

    var detailViewController: DetailViewController? = nil
    var objects = [RepositoryModel]()
    var repositoryPresenter : RepositoryPresenter?
    var pageCount = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        self.repositoryPresenter = RepositoryPresenter()
        self.repositoryPresenter?.viewDelegate = self
        
        getNewPage()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = objects[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.title = object.RepositoryName
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (self.objects.count - 1 == indexPath.row){
            getNewPage()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RepositoryViewCell
        let object = objects[indexPath.row]
        cell.setRepository(rep: object)
        return cell
        
    }

    func setListRepository(list: [RepositoryModel]) {
        self.objects.append(contentsOf: list)
        self.tableView.reloadData();
    }
    
    func getNewPage(){
        self.repositoryPresenter?.getRepository(page: pageCount)
        pageCount += 1
    }
    
    func showMessage(msg: String) {
        let alert = UIAlertController(title: "Atenção", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

