//
//  RepositoryModel.swift
//  DesafioConcrete
//
//  Created by Francisco on 04/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import Foundation
import ObjectMapper

class RepositoryModel : Mappable{
    var RepositoryName : String?
    var Description : String?
    var Stars : Int?
    var Forks : Int?
    var Repository: String?
    var Owner : OwnerModel?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        RepositoryName  <- map["full_name"]
        Description     <- map["description"]
        Repository      <- map["name"]
        Stars           <- map["stargazers_count"]
        Forks           <- map["forks_count"]
        Owner           <- map["owner"]
    }
}

class RepositoryListModel: Mappable{
    required init?(map: Map) {
        
    }

    var items : [RepositoryModel]?
    
    func mapping(map: Map) {
        items       <- map["items"]
    }
}
