//
//  RepositoryPresenterDelegate.swift
//  DesafioConcrete
//
//  Created by Francisco on 04/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import Foundation

protocol RepositoryPresenterDelegate {
    func getRepository(page: Int) 
    func getPullRequest(rep : RepositoryModel)
}
