//
//  DetailViewDelegate.swift
//  DesafioConcrete
//
//  Created by Francisco on 05/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import Foundation

protocol DetailViewDelegate {
    func setDetailObject(pull: [PullModel])
    func showMessage(msg: String)
}
