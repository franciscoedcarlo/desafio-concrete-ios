//
//  ListMainViewDelegate.swift
//  DesafioConcrete
//
//  Created by Francisco on 04/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import Foundation

protocol ListMainViewDelegate{
    func setListRepository(list: [RepositoryModel])
    func showMessage(msg: String)
}
