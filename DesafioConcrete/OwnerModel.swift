//
//  OwnerModel.swift
//  DesafioConcrete
//
//  Created by Francisco on 05/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import Foundation
import ObjectMapper

class OwnerModel : Mappable{
    var UserName : String?
    var UserLogin : String?
    var AvatarURL : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        UserLogin       <- map["login"]
        AvatarURL       <- map["avatar_url"]
    }
}
