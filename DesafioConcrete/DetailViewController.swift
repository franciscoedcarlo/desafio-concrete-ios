//
//  DetailViewController.swift
//  DesafioConcrete
//
//  Created by Francisco on 04/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import UIKit


class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DetailViewDelegate {

    @IBOutlet weak var tableView: UITableView!

    var repositoryPresenter = RepositoryPresenter()
    var listPR = [PullModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.repositoryPresenter.viewDetailDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: RepositoryModel? {
        didSet {
            repositoryPresenter.getPullRequest(rep: detailItem!)
        }
    }

    func setDetailObject(pull: [PullModel]) {
        self.listPR = pull
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listPR.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PullRequestViewCell
        
        let object = listPR[indexPath.row]
        cell.setPullRequest(pull: object)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = NSURL(string: listPR[indexPath.row].URL!)!
        UIApplication.shared.openURL(url as URL)
    }
    
    func showMessage(msg: String) {
        let alert = UIAlertController(title: "Atenção", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

