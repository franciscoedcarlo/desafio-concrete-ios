//
//  PullModel.swift
//  DesafioConcrete
//
//  Created by Francisco on 05/07/17.
//  Copyright © 2017 Francisco. All rights reserved.
//

import Foundation
import ObjectMapper

class PullModel : Mappable{
    var Title : String?
    var Description : String?
    var Created : String?
    var URL : String?
    var User : OwnerModel?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Title       <- map["title"]
        Description <- map["body"]
        User        <- map["user"]
        URL         <- map["html_url"]
        Created     <- map["created_at"]
        
    }
}
